FROM nginx:alpine
# Set working directory
WORKDIR /var

ADD ./nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf

RUN apk update && \
    apk upgrade && \
    apk add git